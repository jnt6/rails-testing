# Help with views
module ApplicationHelper
  def current_user
    nil
  end
  def app_class_name
    'GREPFORMYNAMETOREPLACE'
  end

  def use_webpacker
    true
  end

  def hide_footer
    true
  end

  def nav_links
    links = [
      # menu_link('Normal items', normal_items_path, 'normal_items')
    ]
    return links unless current_user&.admin?

    links + [
      # menu_link('Admin items', admin_items_path, 'admin_items')
    ]
  end

  def menu_link text, path, url_match
    { class: params[:controller].start_with?(url_match) && 'active',
      link: link_to(text, path, class: 'nav-link') }
  end

  def nav_right
    return nil unless current_user&.admin?

    # Admin users should see some deployment details
    url = ENV['CI_PIPELINE_URL']
    text = "BUILD: #{ENV['CI_COMMIT_REF_NAME']} "\
      "#{ENV['CI_COMMIT_SHA']&.[](0..5)} "\
      "#{ENV['CI_BUILD_TIME']}"

    html =
      <<~HTML
        <ul class="nav navbar-nav nav-pills"><li class=false><a target=_blank href=#{url}>
          #{text}
        </a></li></ul>
      HTML
    html.html_safe
  end
end
